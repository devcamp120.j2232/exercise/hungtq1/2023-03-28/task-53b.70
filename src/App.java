import java.text.SimpleDateFormat;
import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Nguyen Van A", true, "Vip");
        Customer customer2 = new Customer("Le Thi B", true, "normal");
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = dateFormat.parse("03-04-2023");
        Visit visit1 = new Visit(customer1, date, 150000.0, 100000.0);
        Visit visit2 = new Visit(customer2, date, 250000.0, 50000.0);
        System.out.println(visit1.toString());
        System.out.println(visit2.toString());
        // Tinh tông phi đã tiêu
        System.out.println(visit1.getTotalExpense());
        System.out.println(visit2.getTotalExpense());
    }
}
