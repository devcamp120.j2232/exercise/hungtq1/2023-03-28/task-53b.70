import java.util.Date;

public class Visit {
  private Customer customer;
  private Date date;
  private Double serviceExpense;
  private Double productExpense;

  public String getName() {
    return customer.getName();
  }

  public Visit(Customer customer, Date date, Double serviceExpense, Double productExpense) {
    this.customer = customer;
    this.date = date;
    this.serviceExpense = serviceExpense;
    this.productExpense = productExpense;
  }

  public Double getServiceExpense() {
    return serviceExpense;
  }

  public void setServiceExpense(Double serviceExpense) {
    this.serviceExpense = serviceExpense;
  }

  public Double getProductExpense() {
    return productExpense;
  }

  public void setProductExpense(Double productExpense) {
    this.productExpense = productExpense;
  }

  public double getTotalExpense() {
    return serviceExpense + productExpense;
  }

  @Override
  public String toString() {
    return "Visit [customer=" + customer + ", date=" + date + ", serviceExpense=" + serviceExpense + ", productExpense="
        + productExpense + "]";
  }

}
